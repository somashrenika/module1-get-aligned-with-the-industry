
1. 
- **Product name**
Pre-Trained Segmentation Models

- **Product link**
   [link](https://www.fritz.ai/features/image-segmentation.html)

- **Product Short description**
This product uses image segmentation to recognize objects and identify exactly which pixels belong to each object.

 
- **Product is combination of features**
Image Segmentation


- **Product is provided by which company?**
fritz.ai




2. 
- **Product name**
FaceVACS-VideoScan

- **Product link**
   [link](https://www.cognitec.com/facevacs-videoscan.html)

- **Product Short description**
FaceVACS-VideoScan recognizes people’s faces in live video streams and video footage, compares them to image databases, and instantly finds known persons.
 
- **Product is combination of features**
1. Face recognition
1. Face Detection


- **Product is provided by which company?**
cognitec
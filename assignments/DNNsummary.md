**Deep Neural Network**
Deep neural network represents the type of machine learning when the system uses many layers of nodes to derive high-level functions from input information. It means transforming the data into a more creative and abstract component.
- It consists of the following layers
1. The first layer is called the Input Layer
1. The last layer is called the Output Layer
1. All layers in between are called Hidden Layers. The word deep means the network join neurons in more than two layers.
![](https://www.oreilly.com/library/view/python-natural-language/9781787121423/assets/70fd1f89-7609-4bdc-86e2-a8786869b8be.png)
- Each Hidden layer is composed of neurons. The neurons are connected to each other. The neuron will process and then propagate the input signal it receives the layer above it. The strength of the signal given the neuron in the next layer depends on the weight, bias and activation function.
- **Input Layer**
- The input layer of a neural network is composed of artificial input neurons, and brings the initial data into the system for further processing by subsequent layers of artificial neurons. The input layer is the very beginning of the workflow.
- **Hidden Layer**
- In neural networks, a hidden layer is located between the input and output of the algorithm, in which the function applies weights to the inputs and directs them through an activation function as the output.
- **Output Layer**
- The output layer in an artificial neural network is the last layer of neurons that produces given outputs for the program.
- Each neuron in a layer and is connected to each neuron in the next layer.  When the inputs are transmitted between neurons, the weights are applied to the inputs along with the bias.
- **Weights** control the signal between two neurons.  In other words, a weight decides how much influence the input will have on the output.
- **Biases**, which are constant, are an additional input into the next layer. Bias units are not influenced by the previous layer but they do have outgoing connections with their own weights.

![](https://missinglink.ai/wp-content/uploads/2018/11/Frame-1-e1542095079559.png)

-A gradient is a vector. Its components consist of the partial derivatives of a function and it points in the direction of the greatest rate of increase of the function. If there is a function  f(x1,...xn) , its gradient would consist of n partial derivatives and would represent the vector field.

- **Sigmoid Function**
A sigmoid function is a type of activation function, and more specifically defined as a squashing function. Squashing functions limit the output to a range between 0 and 1, making these functions useful in the prediction of probabilities.

![](https://qph.fs.quoracdn.net/main-qimg-6b67bea3311c3429bfb34b6b1737fe0c)


- **Cost Function**Cost function of a neural network is a generalization of the cost function of the logistic regression.
- **Back Propagation**
Backpropagation algorithm is based on the repeated application of the error calculation used for gradient descent similar to the regression techniques, and since it is repeatedly applied in the reverse order starting from output layer and continuing towards input layer.
















